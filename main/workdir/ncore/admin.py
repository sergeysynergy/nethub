from django.contrib import admin
#
from .models import Project, Slide


admin.site.register(Project)
admin.site.register(Slide)
