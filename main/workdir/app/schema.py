import graphene
import graphql_jwt

import users.schema
import ncore.schema


class Query(
    users.schema.Query,
    ncore.schema.Query,
    graphene.ObjectType,
):
    pass


class Mutation(
    users.schema.Mutation,
    ncore.schema.Mutation,
    graphene.ObjectType,
):
    tokenAuth = graphql_jwt.ObtainJSONWebToken.Field()
    verifyToken = graphql_jwt.Verify.Field()
    refreshToken = graphql_jwt.Refresh.Field()


schema = graphene.Schema(
    query=Query, 
    mutation=Mutation
)

