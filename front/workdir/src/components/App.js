import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
//
import { ROLE, media } from '../constants'
import Header from './Header'
import Explore from './Explore'
import Tutorial from './Tutorial'
//
import Signin from './includes/Signin'
import Register from './includes/Register'
import Bottom from './includes/Bottom'
import Marketing from './includes/Marketing'
import User from './includes/User'
import UserNet from './includes/UserNet'
/* End of import section */

const StyledApp = styled.div`
  .content {
    .header {
      padding: 2em 20%;
      border-bottom: solid 1px #ccc;
    }
    .body {
      margin: 2em 20%;
      h2 {
        margin: 1em 0;
        font-size: 1.4em;
      }
    }
  }

  ${media.tablet`
    .content {
      .header {
        padding: .6em .4em;
      }
      .body {
        margin: .6em .4em;
      }
    }
  `}
`
const {USER, ANONYM} = ROLE

export default class App extends Component {
  getRole = (userToken, userId) => {
    /*
    console.log('typeof userToken', typeof userToken);
    console.log('userToken', userToken);
    console.log('typeof userId', typeof userId);
    console.log('userId', userId);
    */
    if (typeof userToken !== 'string') {
      return ANONYM
    }
    if (userToken === 'undefined' || userToken === '') {
      return ANONYM
    }
    return USER
  }

  constructor(props) {
    super()

    this.state = {
      userToken: localStorage.getItem('USER_TOKEN'),
      userId: localStorage.getItem('USER_ID'),
      username: localStorage.getItem('USER_NAME'),
      role: this.getRole(localStorage.getItem('USER_TOKEN'),  localStorage.getItem('USER_ID')),
    }

    /*
    console.log('state token', this.state.userToken);
    console.log('state userId', this.state.userId);
    console.log('state name', this.state.username);
    console.log('state role', this.state.role);
    console.log('========================================');
    */
  }


  saveUserData = (id, token, username) => {
    // Updating local storage user data
    localStorage.setItem('USER_ID', id)
    localStorage.setItem('USER_TOKEN', token)
    localStorage.setItem('USER_NAME', username)
    this.setState({
      userId: id,
      userToken: token,
      username: username,
      role: USER,
    })
  }

  logOut = () => {
    // Updating local storage user data
    localStorage.setItem('USER_ID', '')
    localStorage.setItem('USER_TOKEN', '')
    localStorage.setItem('USER_NAME', '')
    this.setState({userId: '', userToken: '', role: ANONYM})
  }

  render() {
    /*
    console.log('render token', this.state.userToken);
    console.log('render userId', this.state.userId);
    console.log('render role', this.state.role);
    console.log('=========================================');
    */

    return <StyledApp>
      <Header
        {...this.props}
        userID={this.state.userId}
        username={this.state.username}
        role={this.state.role}
        _logOut={this.logOut}
      />
      <Switch>
        <Route exact path="/" component={(props) => (
          <Marketing {...props} />)} />
        <Route exact path="/sign_in" component={(props) => (
          <Signin
            _saveUserData={this.saveUserData}
          {...props} />)}
        />
        <Route exact path="/register" component={(props) => (
          <Register
            _saveUserData={this.saveUserData}
          {...props} />)}
        />
        <Route exact path="/explore" component={(props) => (
          <Explore
          {...props} />)}
        />
        <Route path="/users/:username/:title" component={(props) => (
          <UserNet
            userId={this.state.userId}
            username={this.state.username}
          {...props} />)}
        />
        <Route path="/users/:username" component={(props) => (
          <User
            username={this.state.username}
          {...props} />)}
        />
        <Route exact path="/tutorial" component={(props) => (
          <Tutorial
          {...props} />)}
        />
      </Switch>
      <Bottom />
    </StyledApp>
  }
}
