import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import {
  Jumbotron, Input, FormGroup, Button, Label, Alert,
} from 'reactstrap'
//
import { media } from '../../constants'
// import SigninUserMutation from '../../mutations/SigninUserMutation'
import CreateUserMutation from '../../mutations/CreateUserMutation'
/* End of import section */

const StyledRegister = styled.div`
  // border: dashed 2px green;
  margin: 2em 0 0 0;
  .jumbotron {
    max-width: 350px;
    margin: 0 auto;
  }
  button {
    margin: 0 0 1em 0;
  }
  a.Link:hover,
  a.Link {
    padding: 0.2em 1em;
    text-decoration: underline;
    color: inherit;
    cursor: pointer;
  }

  ${media.tablet`
    margin: 0 .6em;
  `}
`


export default class Register extends Component {
  state = {
    email: '',
    emailConfirm: '',
    password: '',
    username: '',
    warning: '',
  }

  render() {
    return <form>
      <StyledRegister>
        <Jumbotron>
          <h2><Link to='/sign_in'>Sign in</Link> / Register</h2>
          <FormGroup>
            <Label>Username</Label>
            <Input
              type="text"
              value={this.state.username}
              placeholder="username"
              onChange={(e) => this.setState({ username: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label>Email</Label>
            <Input
              type="text"
              value={this.state.email}
              placeholder="email address"
              onChange={(e) => this.setState({ email: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label>Confirm email</Label>
            <Input
              type="text"
              value={this.state.emailConfirm}
              placeholder="confirm email address"
              onChange={(e) => this.setState({ emailConfirm: e.target.value })}
            />
          </FormGroup>
          <FormGroup>
            <Label>Password</Label>
            <Input
              type="password"
              value={this.state.password}
              placeholder='password'
              onChange={(e) => this.setState({ password: e.target.value })}
              autoComplete=""
            />
          </FormGroup>
          <Button
            color="primary"
            onClick={() => this.confirm()}
          >
            Register
          </Button>
          {this.state.warning.length > 0 &&
            <Alert color="warning">
              {this.state.warning}
            </Alert>
          }
        </Jumbotron>
      </StyledRegister>
    </form>
  }

  confirm = () => {
    const { username, email, emailConfirm, password } = this.state
    if (username.length < 2 || email.length < 2 || password.length < 2) {
      this.setState({warning: 'Please input correct register data'})
    } else if (email !== emailConfirm) {
      this.setState({warning: 'Emails not match'})
    } else {
      CreateUserMutation(username, email, password,
            (result, data, errors) => {
        if (result) {
          this.props._saveUserData(data.id, data.token, data.username)
          this.props.history.push(`/users/${data.username}`)
        } else {
          if (errors.tokenAuth === null ||
              typeof errors.tokenAuth.token !== 'undefined') {
            this.setState({warning: 'User already exists!'})
          } else {
            // this.setState({warning: 'Ooops!'})
          }
        }
      })
    }
  }
}
Register.propTypes = {
  _saveUserData: PropTypes.func,
}
