import React from 'react'
import styled from 'styled-components'
import ReactMarkdown from 'react-markdown/with-html'
//
import { media } from '../../constants'
import bg from './bg.jpg'
/* End of import section */

const StyledMarketing = styled.div`
  padding: 2em 4em;
  font-size: 2em;
  background-image: url(${bg});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  color: white;

  ${media.tablet`
  `}
`

const input = "# NetHub - is a multifunctional cloud service for working with neural networks\n \
- Cloud storage for neural networks and, as a result, a database of neural networks with flexible access settings;\n \
- a Set of Open Source tools for working with neural networks, including the unique development team of Nethub;\n \
- a Single standard of data and metadata for developers of neural networks;\n \
- a Commercial platform for developers to interact with customers.\n \
"


export default () => <StyledMarketing>
  <ReactMarkdown source={input} />
</StyledMarketing>
