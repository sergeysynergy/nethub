import React from 'react'
import styled from 'styled-components'
import ReactMarkdown from 'react-markdown/with-html'
import CodeBlock from './CodeBlock'
//
import { media } from '../../constants'
/* End of import section */

const StyledTutorial = styled.div`

  tr {
    border-top: 1px solid #c6cbd1;
    background: #fff;
  }

  th,
  td {
    padding: 6px 13px;
    border: 1px solid #dfe2e5;
  }

  table tr:nth-child(2n) {
    background: #f6f8fa;
  }

  ${media.tablet`
  `}
`

const input = `
### Connect to NetHub server via Nethub-Manager

-----

| Platform  | Support         | Library | Support        |
| --------- | -------         | ------- | -------        |
| Linux     | ✔               | NetPy   | ✔              |
| Windows   | In Progress...  | PyTorch | In Progress... |
| MacOs     | In Progress...  | Keras   | In Progress... |

-----

### Install Nethub-Manager

-----

\`\`\`bash
pip install nethub-manager
\`\`\`


First, you need to initialize all settings with command _init_:\n
\`\`\`bash
nhm init
\`\`\`

Then, you need to login (you need to register on your nethub server):\n
\`\`\`bash
nhm login
\`\`\`

Okay, let's create first net (NetPy):\n
\`\`\`bash
nhm newnet my_first_net
\`\`\`

And write your first commit!\n
\`\`\`bash
Commit: My first commit!
\`\`\`

You can see your net in web-browser\n
![NetHub](pics/nethub.png)


Let's change our network init file (\`network.py\`).\n
Add some neurons:\n

\`\`\`python

''' It's network init file '''

from netpy.nets import FeedForwardNet
from netpy.modules import LinearLayer, SigmoidLayer, FullConnection


net = FeedForwardNet(name = 'my_first_net')

# Add your layers here
input_layer = LinearLayer(3)
hidden_layer = SigmoidLayer(2)
output_layer = SigmoidLayer(1)

# Add your layers to the net here
net.add_Input_Layer(input_layer)
net.add_Layer(hidden_layer)
net.add_Output_Layer(output_layer)

# Add your connections here
con_in_hid = FullConnection(input_layer, hidden_layer)
con_hid_out = FullConnection(hidden_layer, output_layer)

# Add your connections to the net here
net.add_Connection(con_in_hid)
net.add_Connection(con_hid_out)

# Save your net
net.save()

\`\`\`

Push your changes to server:\n
\`\`\`bash
cd my_first_net
nhm add network.py
nhm commit
nhm push
\`\`\`

To upload your local net to server:\n
\`\`\`bash
nhm upload my_first_net
\`\`\`

`

export default () => <StyledTutorial>
  <ReactMarkdown
    source={input}
    renderers={{code: CodeBlock}}
  />
</StyledTutorial>
