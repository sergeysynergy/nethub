import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import styled from 'styled-components'
import { NavLink } from 'reactstrap'
import { NavLink as RRNavLink } from 'react-router-dom'
import { Icon } from 'react-icons-kit'
import { androidShareAlt } from 'react-icons-kit/ionicons/androidShareAlt'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'
//
import { media } from '../../constants'
/* End of import section */


const StyledHeaderIn = styled.div`
  // border: dashed 2px green;
  border-bottom: solid 2px #ccc;
  .navbar {
    padding: .1em .8em .1em .4em;
    background-color: #cc66ff;
    a, a:visited {
      color: white;
      font-weight: bold;
    }
  }
  .logo {
    margin: 0 .6em 0 0;
    color: orange;
  }
  .icon {
    font-size: 18px;
    font-weight: bold;
    padding: 2px 8px;
    margin: 0 .6em 0 0;
    background-color: #aaa;
    color: #fff;
    border-radius: 2em;
    text-transform: uppercase;
  }
  .logout {
    cursor:pointer;
  }

  ${media.tablet`
    width: 100%;
  `}
`

class Header extends Component {
  state = {
      isOpen: false,
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  logOut = () => {
    this.props.history.push(`/`)
    this.props._logOut()
  }


  render() {
    // console.log('props', this.props);

    return (
      <StyledHeaderIn>
        <Navbar dark expand="md">
          <Nav navbar>
            <NavLink
              to="/"
              activeClassName="active"
              tag={RRNavLink}
            >
              <Icon icon={androidShareAlt} className='logo' />
              <span className='logo'>NetHub</span>
            </NavLink>
            <NavLink
              to={`/users/${this.props.username}`}
              activeClassName="active"
              tag={RRNavLink}
            >
              Projects
            </NavLink>
          </Nav>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavLink
                to="/explore"
                activeClassName="active"
                tag={RRNavLink}
              >
                Explore
              </NavLink>
              <NavLink
                to="/tutorial"
                activeClassName="active"
                tag={RRNavLink}
              >
                Tutorial
              </NavLink>
              <NavItem>
                <NavLink disabled href="#">|</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  {/*
                  <span className='icon'>
                    {this.props.username[0]}
                  </span>
                  */}
                  {this.props.username}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem
                    className='logout'
                    onClick={this.logOut}
                    >
                    LogOut
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </StyledHeaderIn>
    )
  }
}


export default withRouter(Header)
