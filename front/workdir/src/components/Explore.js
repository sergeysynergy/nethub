import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
//
import environment from '../Environment'
import { media } from '../constants'
/* End of import section */

const StyledExplore = styled.div`
  ${media.tablet`
  `}
`

const StyledNetsRow = styled.div`
  margin: 0 0 1em 0;
  padding: 1em 0;
  font-size: 1.4em;
  border-bottom: solid 1px #ccc;
  .meta {
    font-size: .7em;
    color: #777;
    a {
      color: #777;
    }
    .build {
      margin: 0 0 0 1em;
    }
  }

  ${media.tablet`
  `}
`

const query = graphql`
  query ExploreQuery {
    allProjects {
      id
      title
      library
      user {
        id
        username
      }
    }
  }
`


export default class Explore extends Component {
  render() {
    return <QueryRenderer
      environment={environment}
      query={query}
      variables={{}}
      render={({error, props}) => {
        if (error) {
          return <div>{error.message}</div>
        } else if (props) {
          let projects = []
          if (props.allProjects != null) {
            projects = props.allProjects.map((item, key) =>(
              <NetsRow
                key={key}
                title={item.title}
                library={item.library}
                username={item.user.username}
                />
            ))
          }

          return <StyledExplore className="content">
            <div className="header">
              <h1>Explore neuronets</h1>
            </div>
            <div className="body">
              <h2>Popular repositories</h2>
              {projects}
            </div>
          </StyledExplore>
        }
        return <div></div>
      }}
    />
  }
}


const NetsRow = ({title, username, library}) => <StyledNetsRow>
  <Link to={'/users/' + username + '/' + title}>
    {username} /
    <strong>{title}</strong>
  </Link>
  <div className="meta">
    <span className="library">
      Library: {library}
    </span>
    <span className="build">
      Build by: <Link to={'/users/' + username}>{username}</Link>
    </span>
  </div>
</StyledNetsRow>
