import { commitMutation, graphql, } from 'react-relay'
import environment from '../Environment'

const mutation = graphql`
  mutation DeleteProjectMutation($input: DeleteProjectInput!) {
    deleteProject(input: $input) {
      owner {
        id
        username
      }
    }
  }
`

export default (id, callback ) => {
  const variables = {
    input: {
      id,
    },
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: () => {
        callback()
      },
      onError: err => console.error(err),
    },
  )
}
