import {
  commitMutation,
  graphql
} from 'react-relay'
//
import environment from '../Environment'


const mutation = graphql`
  mutation CreateForkMutation($input: CreateForkInput!) {
    forkProject(input: $input) {
      forkProject {
        id
      }
    }
  }
`


export default (projectId, callback) => {
  const variables = {
    input: {
      projectId,
    },
    clientMutationId: '',
  }

  commitMutation(
    environment,
    {
      mutation,
      variables,
      onCompleted: (response) => {
        callback(response)
      },
      onError: err => console.error(err),
    },
  )
}
