import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
//
import { media } from '../../constants'


const StyledHeader = styled.div`
  background-color: #bbb;
  padding: 1em 4em .6em 4em;
  .user {
    font-size: 24px;
    font-weight: bold;
    .icon {
      padding: 2px 20px;
      background-color: #eee;
      border-radius: 2em;
      text-transform: uppercase;
    }
    .name {
      padding: 0 0 0 .5em;
    }
  }
  .menu {
    padding: .4em 0 0 0;
    font-size: 1.4em;
  }

  ${media.tablet`
  `}
`


export default ({username, nets=false}) =>
  <StyledHeader>
    <div className='user'>
      <span className='icon'>
        {username[0]}
      </span>
      <span className='name'>
        {username}
      </span>
    </div>
    <div className='menu'>
      {nets ?
        <Link to={`/users/${username}`}>
          Neuronets
        </Link>
      :
        <>Neuronets</>
      }
    </div>
  </StyledHeader>
