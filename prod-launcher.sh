#!/bin/bash

if [[ $1 = "--help" ]]; then
  echo "Commands:"
  echo "--help - this help"
  echo "init - ititialize NNetHub, needed at first run"
  echo "start - start NNetHub"
  exit 1
fi

if [[ $1 = "init" ]]; then
  clear
  # Build all project containers:
  docker-compose build

  # Start all project containers:
  docker-compose -f docker-compose.yml -f docker-production.yml up -d

  # Init db
  # docker exec nethub_main_maindb psql -U postgres -f maindb.sql

  # Initialize main django container
  docker exec nethub_main_django_prod ./run.sh init

  # Initialize git container
  docker exec nethub_main_git_prod yarn

  # Initialize front nodejs container
  docker exec nethub_front_nodejs_prod ./run.sh init

  ./prod-launcher.sh start
  exit 1
fi

if [[ $1 = "start" ]]; then
  # clear
  # Start all project containers:
  docker-compose -f docker-compose.yml -f docker-production.yml up -d

  # Start Django develop web-server
  docker exec -d nethub_main_django_prod ./runprod.sh

  # Start Flower - Celery workers monitor
  # docker exec -d nethub_main_django celery -A app flower

  # Start Git http server
  docker exec -d nethub_main_git_prod ./run.sh

  # Start React develop web-server
  docker exec -d nethub_front_nodejs_prod ./run.sh

  exit 1
fi

# DEFAULT COMMANDS WITHOUT ARGUMENTS
./prod-launcher.sh --help
